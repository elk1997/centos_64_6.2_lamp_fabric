from fabric.api import run
from fabric.api import sudo
from fabric.contrib.files import exists

import os.path

mysql_password = 'vagrant'

def install_yum(pkg):
    pkgs = " ".join(pkg)
    sudo('yum install -y ' + pkgs)

def general():
    if exists('/etc/yum.repos.d/epel.repo') is False:
        sudo('rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm')
    
    if exists('/etc/yum.repos.d/remi.repo') is False:
        sudo('rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm')
        sudo('cp /vagrant/remi.repo /etc/yum.repos.d/remi.repo')

    #sudo('yum update -y')
      
    pkg = ["tree", "ntp", "vim-enhanced", "mlocate", "wget", "libxml2", "libxml2-devel", "gcc", "git", "postfix", "perl-CPAN", "libicu-devel"]
    install_yum(pkg)

    sudo('/etc/init.d/iptables stop')
    sudo('chkconfig iptables off')


def php():
    pkg = ["php", "php-mbstring", "php-mysql", "php-gd", "php-pear", "php-common", "php-devel"]
    if exists('/etc/php.ini') is False:
        install_yum(pkg)
        sudo('cp /vagrant/php.ini /etc/php.ini')

def httpd():
    pkg = ["httpd", "httpd-devel", "openssl", "openssl-devel"]
    if exists('/etc/httpd/conf/httpd.conf') is False:
        install_yum(pkg)
        sudo('cp /vagrant/httpd.conf /etc/httpd/conf/httpd.conf')
        sudo('/etc/init.d/httpd start')
        sudo('chkconfig httpd on')
    sudo('/etc/init.d/httpd start')

def mysql():
    pkg = ["mysql-server", "mysql"]
    if exists('/etc/init.d/mysqld') is False:
        install_yum(pkg)
        sudo('/etc/init.d/mysqld start')  
        sudo('chkconfig mysqld on')
        sudo ('mysqladmin -u root password ' + mysql_password)
        sudo('/etc/init.d/mysqld restart')
        sudo('mysql -u root -p' + mysql_password + ' --execute="DELETE FROM mysql.user WHERE user=\'\' or password=\'\'"');
        sudo('mysql -u root -p' + mysql_password + ' --execute="update mysql.user set Host = \'%\'"');
        sudo('/etc/init.d/mysqld restart')
